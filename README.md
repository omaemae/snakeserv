# SnakeServ

![Python Version](https://img.shields.io/badge/Python-3.10+-informational)
![Minecraft Version](https://img.shields.io/badge/Minecraft-1.19-yellowgreen)
![GitLab issues](https://img.shields.io/gitlab/issues/open/omaemae/snakeserv)
![License](https://img.shields.io/gitlab/license/omaemae/snakeserv)

SnakeServ is an experimental Minecraft server implementation written in Python.

Its current goal is to have a small server that is able to at least load in one player.

## Supported Minecraft versions

- 1.19 (protocol version 759)

## Supported Python versions

- 3.10 and above

import uuid
from minecraft.entity import Entity
from minecraft.slot import InventorySlot


class Player(Entity):
    socket = None
    type = "minecraft:player"
    last_tp_id: int | None = None
    inventory = [InventorySlot()] * 45

    def __init__(self, name: str, uid: uuid.UUID | None):
        self.name: str = name
        self.uuid: uuid.UUID = uid or uuid.uuid4()
        self.inventory[36].present = True
        self.inventory[36].item_id = "stone"
        self.inventory[36].count = 1

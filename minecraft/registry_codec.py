from pynbt import NBTFile, TAG_List, TAG_Compound, TAG_Byte, TAG_Int, TAG_Float, TAG_Double, TAG_String

registry_codec = NBTFile(value={
    "minecraft:worldgen/biome": TAG_Compound({
        "type": TAG_String("minecraft:worldgen/biome"),
        "value": TAG_List(TAG_Compound, [])
    }),
    "minecraft:dimension_type": TAG_Compound({
        "type": TAG_String("minecraft:dimension_type"),
        "value": TAG_List(TAG_Compound, [
            {
                "name": TAG_String("minecraft:overworld"),
                "id": TAG_Int(0),
                "element": TAG_Compound({
                    "piglin_safe": TAG_Byte(0),
                    "has_raids": TAG_Byte(1),
                    "monster_spawn_light_level": TAG_Compound({
                        "value": TAG_Compound({
                            "max_inclusive": TAG_Int(7),
                            "min_inclusive": TAG_Int(0),
                        }),
                        "type": TAG_String("minecraft:uniform")
                    }),
                    "monster_spawn_block_light_limit": TAG_Int(0),
                    "natural": TAG_Byte(1),
                    "ambient_light": TAG_Float(0.0),
                    "infiniburn": TAG_String("#minecraft:infiniburn_overworld"),
                    "respawn_anchor_works": TAG_Byte(0),
                    "has_skylight": TAG_Byte(1),
                    "bed_works": TAG_Byte(1),
                    "effects": TAG_String("minecraft:overworld"),
                    "min_y": TAG_Int(-64),
                    "height": TAG_Int(384),
                    "logical_height": TAG_Int(384),
                    "coordinate_scale": TAG_Double(1.0),
                    "ultrawarm": TAG_Byte(0),
                    "has_ceiling": TAG_Byte(0),
                })
            }
        ])
    }),
    "minecraft:chat_type": TAG_Compound({
        "type": TAG_String("minecraft:chat_type"),
        "value": TAG_List(TAG_Compound, [
            {
                "name": TAG_String("minecraft:chat"),
                "id": TAG_Int(0),
                "element": TAG_Compound({
                    "chat": TAG_Compound({
                        "decoration": TAG_Compound({
                            "parameters": TAG_List(TAG_String, ["sender", "content"]),
                            "translation_key": TAG_String("chat.type.text"),
                            "style": TAG_Compound({})
                        })
                    }),
                    "narration": TAG_Compound({
                        "decoration": TAG_Compound({
                            "parameters": TAG_List(TAG_String, ["sender", "content"]),
                            "translation_key": TAG_String("chat.type.text.narrate"),
                            "style": TAG_Compound({})
                        })
                    }),
                    "priority": TAG_String("chat")
                })
            }
        ])
    })
})

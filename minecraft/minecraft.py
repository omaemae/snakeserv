import collections


class Minecraft:
    motd = "A Minecraft Server"
    players = collections.deque()
    max_players = 10
    favicon: bytes | None = None
    hardcore = False
    game_mode = 0
    prev_game_mode = -1
    dimensions = [
        "minecraft:overworld"
    ]
    seed = "idk"
    view_distance = 10
    simulation_distance = 32
    reduced_debug = False
    immediate_respawn = False
    is_debug = False
    is_flat = True
    spawn_pos = (0, 1, 0)
    spawn_angle = 0.0
    time = 1000

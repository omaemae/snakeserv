import struct
from pynbt import NBTFile
from mcnetty import protocol


class InventorySlot:
    present = False
    item_id: str = "minecraft:empty"
    count: int = 0
    nbt: NBTFile | None = None

    def to_bytes(self) -> bytes:
        data = struct.pack('?', self.present)
        if self.present:
            data += protocol.construct_string(self.item_id)
            data += struct.pack('b', self.count)
            data += protocol.construct_nbt(self.nbt)

import random


class Entity:
    type = "pymc:unknown"
    id = random.randint(0, 0x7fffffff)
    x = 0
    y = 1
    z = 0
    yaw = 0
    pitch = 0

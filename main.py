#!/usr/bin/env python3
import sys
import time
from threading import Thread

import mcnetty
import minecraft
import socketserver

mc = minecraft.Minecraft()
mc.motd = f"hello from Python {'.'.join([str(i) for i in sys.version_info[:3]])}"
with open("server-data/favicon.png", 'rb') as favicon:
    mc.favicon = favicon.read()


class MCRequestHandler(socketserver.BaseRequestHandler):
    protocol_state = 0
    socket_open = True
    last_ping_id: int = None
    player: minecraft.Player = None
    keepalive_thread: Thread = None
    protocol_version: int = 0

    def send_keepalive(self):
        while self.socket_open:
            (should_close, data, new_id) = mcnetty.play.send_keep_alive(self.last_ping_id)
            self.request.sendall(data)
            print("sent PING to client")
            if should_close:
                self.close_socket()
                return
            time.sleep(10)

    def close_socket(self):
        if not self.socket_open:
            print("socket already closed for", self.client_address[0], "; ignoring redundant call")
            return
        print("closing socket for", self.client_address[0])
        self.socket_open = False
        self.request.close()
        if self.player:
            mc.players.remove(self.player)

    def handle(self):
        data = b''
        while self.socket_open:
            data = self.request.recv(5)
            if len(data) == 0:
                break
            (length, data) = mcnetty.consume_varint(data)
            receive_length = length - len(data)
            if receive_length > 0:
                data += self.request.recv(receive_length)
            (packet_id, data) = mcnetty.consume_varint(data)
            self.process_request(packet_id, data)
        self.close_socket()  # formally close the socket after breaking out of the while loop, just in case

    def process_request(self, packet_id: int, data: bytes):
        print(f"{self.client_address[0]}: {hex(packet_id)} {data}")

        if packet_id == 0 and self.protocol_state == 0:  # Handshake
            (protocol_version, data) = mcnetty.consume_varint(data)
            self.protocol_version = protocol_version
            (hostname, data) = mcnetty.consume_string(data, 255)
            (port, data) = mcnetty.consume_struct_format(data, 'H')  # unsigned short
            (state, data) = mcnetty.consume_varint(data)
            print(f"handshake: protocol={protocol_version}, {hostname}:{port}, state={state}")
            self.protocol_state = state
            return

        if self.protocol_state == 1:
            match packet_id:
                case 0:  # status request
                    self.request.sendall(mcnetty.serverList.handle_status(mc))
                case 1:  # ping
                    self.request.sendall(mcnetty.serverList.handle_ping(data))
                    self.close_socket()

        if self.protocol_state == 2:
            match packet_id:
                case mcnetty.login.Events.LOGIN_START.value:
                    if mcnetty.PROTOCOL_VERSION > self.protocol_version:
                        self.request.sendall(mcnetty.login.send_disconnect(
                            f"Client is too old ({self.protocol_version}, I am on {mcnetty.PROTOCOL_VERSION}!)"
                        ))
                        self.close_socket()
                        return
                    if mcnetty.PROTOCOL_VERSION < self.protocol_version:
                        self.request.sendall(mcnetty.login.send_disconnect(
                            f"Client is too new ({self.protocol_version}, I am on {mcnetty.PROTOCOL_VERSION}!)"
                        ))
                        self.close_socket()
                        return
                    data, player = mcnetty.login.handle_login_start(data, mc)
                    player.socket = self.request
                    self.player = player
                    self.request.sendall(data)
                    self.protocol_state = 3  # Play
                    self.request.sendall(mcnetty.play.send_login(player, mc))
                    self.keepalive_thread = Thread(target=self.send_keepalive)
                    self.keepalive_thread.start()
                    self.request.sendall(mcnetty.play.send_update_time(mc))

        if self.protocol_state == 3:
            match packet_id:
                case mcnetty.play.Events.PONG.value:
                    (new_id, data) = mcnetty.consume_struct_format(data, 'q')
                    self.last_ping_id = new_id
                    print("recv PONG from client")


if __name__ == "__main__":
    with socketserver.ThreadingTCPServer(('0.0.0.0', 25565), MCRequestHandler) as server:
        print("server up Lol")
        try:
            server.serve_forever()
        except KeyboardInterrupt:
            server.shutdown()

import io
import uuid
import struct

from pynbt import NBTFile

SEGMENT_BITS = 0x7f
CONTINUE_BIT = 0x80


class VarBaseTooBigException(Exception):
    pass


class StringTooLongException(Exception):
    pass


def read_var_base(data: bytes, size: int):
    value = 0
    bit_position = 0
    byte_position = 0
    while True:
        current_byte = data[byte_position]
        value += (current_byte & SEGMENT_BITS) << bit_position
        if (current_byte & CONTINUE_BIT) == 0:
            break
        bit_position += 7
        byte_position += 1
        if bit_position >= size:
            raise VarBaseTooBigException(f"expected variable type to be {size} bits, got {bit_position} bits")

    return value, byte_position


def consume_varint(data: bytes):
    (value, byte) = read_var_base(data, 32)
    return value, data[byte + 1:]


def consume_varlong(data: bytes):
    (value, byte) = read_var_base(data, 64)
    return value, data[byte + 1:]


def consume_struct_format(data: bytes, fmt: str):
    fmt = '>' + fmt
    size = struct.calcsize(fmt)
    value = struct.unpack(fmt, data[:size])
    return value[0], data[size:]


def consume_string(data: bytes, max_length=255):
    (length, data) = consume_varint(data)
    if length > max_length:
        raise StringTooLongException(f"expected max length {max_length}, got {length}")
    string = data[:length].decode()
    return string, data[length:]


def consume_bytes(data: bytes, length: int):
    return data[:length], data[length:]


def consume_uuid(data: bytes):
    return uuid.UUID(bytes=data[:16]), data[16:]


def write_var_base(value: int):
    data = b''
    while True:
        if (value & ~SEGMENT_BITS) == 0:
            data += value.to_bytes(1, byteorder='little')
            return data

        data += ((value & SEGMENT_BITS) | CONTINUE_BIT).to_bytes(1, byteorder='little')
        value >>= 7


def construct_packet(packet_id: int, data: bytes):
    packet_data = write_var_base(packet_id) + data
    return write_var_base(len(packet_data)) + packet_data


def construct_string(string: str) -> bytes:
    return write_var_base(len(string)) + string.encode()


def construct_nbt(nbt: NBTFile) -> bytes:
    data = io.BytesIO()
    nbt.save(data)
    return data.getvalue()


def construct_position(x: int, y: int, z: int) -> bytes:
    value = ((x & 0x3FFFFFF) << 38) | ((z & 0x3FFFFFF) << 12) | (y & 0xFFF)
    return struct.pack('>Q', value)

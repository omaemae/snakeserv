import sys

PROTOCOL_VERSION = 759  # 1.19 = 759
BRAND = f"SnakeServ on Python {'.'.join([str(i) for i in sys.version_info[:3]])}"
DISPLAY_VERSION = f"SnakeServ 1.19"

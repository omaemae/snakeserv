import hashlib
import io
import enum
import random
import struct
import time
import json

from pynbt import NBTFile

import minecraft
from mcnetty import protocol


class Events(enum.Enum):
    SET_CONTAINER_CONTENT = 0x11
    PLUGIN_MESSAGE = 0x15
    PONG = 0x11
    DISCONNECT = 0x17
    PING = 0x1E
    LOGIN = 0x23
    SYNCHRONIZE_PLAYER_POS = 0x36
    SET_DEFAULT_SPAWN_POS = 0x4A
    UPDATE_TIME = 0x59


def send_disconnect(reason: str):
    return protocol.construct_packet(Events.DISCONNECT.value,
                                     protocol.construct_string(json.dumps(
                                         {
                                             "text": reason
                                         }
                                     )))


def send_login(player: minecraft.Player, mc: minecraft.Minecraft):
    registry_nbt = io.BytesIO()
    minecraft.registry_codec.save(registry_nbt)
    registry_nbt = registry_nbt.getvalue()

    data = struct.pack('>i', player.id)
    data += struct.pack('?', mc.hardcore)
    data += struct.pack('B', mc.game_mode)
    data += struct.pack('b', mc.prev_game_mode)
    # Dimensions array
    data += protocol.write_var_base(len(mc.dimensions))
    for dimension in mc.dimensions:
        data += protocol.construct_string(dimension)
    data += registry_nbt  # Registry Codec NBT
    data += protocol.construct_string("minecraft:overworld")  # Dimension Type
    data += protocol.construct_string("minecraft:overworld")  # Dimension Name
    # Hashed Seed
    data += hashlib.sha256(mc.seed.encode()).digest()[:8]
    data += protocol.write_var_base(mc.max_players)
    data += protocol.write_var_base(mc.view_distance)
    data += protocol.write_var_base(mc.simulation_distance)
    data += struct.pack('?', mc.reduced_debug)
    data += struct.pack('?', not mc.immediate_respawn)  # Enable respawn screen
    data += struct.pack('?', mc.is_debug)  # is debug world?
    data += struct.pack('?', mc.is_flat)  # is superflat world?
    data += struct.pack('?', False)  # has death location?
    return protocol.construct_packet(Events.LOGIN.value, data)


def send_plugin_channel(channel: str, data: bytes):
    return protocol.construct_packet(Events.PLUGIN_MESSAGE.value, protocol.construct_string(channel) + data)


def send_container_content(player: minecraft.Player, window=0):
    data = struct.pack('B', window)
    data += protocol.write_var_base(0)  # state ID, dont care rn tbh
    data += protocol.write_var_base(len(player.inventory))
    for slot in player.inventory:
        data += slot.to_bytes()
    data += minecraft.InventorySlot().to_bytes()  # carried item
    return protocol.construct_packet(Events.SET_CONTAINER_CONTENT.value, data)


def send_sync_player_position(player: minecraft.Player):
    data = struct.pack('>d', player.x)
    data += struct.pack('>d', player.y)
    data += struct.pack('>d', player.z)
    data += struct.pack('>f', player.yaw)
    data += struct.pack('>f', player.pitch)
    data += struct.pack('b', 0)  # Flags
    player.last_tp_id = random.randint(0, 0xFFFF)
    data += protocol.write_var_base(player.last_tp_id)
    data += struct.pack('?', False)  # Dismount Vehicle
    return protocol.construct_packet(Events.SYNCHRONIZE_PLAYER_POS.value, data)


def send_default_spawn_pos(mc: minecraft.Minecraft):
    data = protocol.construct_position(*mc.spawn_pos)
    data += struct.pack('>f', mc.spawn_angle)
    return protocol.construct_packet(Events.SET_DEFAULT_SPAWN_POS.value, data)


def send_update_time(mc: minecraft.Minecraft):
    data = struct.pack('>q', 0)
    data += struct.pack('>q', mc.time)
    return protocol.construct_packet(Events.UPDATE_TIME.value, data)


def send_keep_alive(last_id: int | None):
    new_id = round(time.time() * 1000)
    if last_id and new_id > last_id + (30 * 1000):
        return True, send_disconnect("No keepalive response within >30s"), new_id

    return False, protocol.construct_packet(Events.PING.value, struct.pack('>q', new_id)), new_id

import base64
import json
from minecraft import Minecraft
from mcnetty import protocol
from mcnetty.version import DISPLAY_VERSION, PROTOCOL_VERSION


def handle_status(mc: Minecraft) -> bytes:
    json_data = {
        "version": {
            "name": DISPLAY_VERSION,
            "protocol": PROTOCOL_VERSION,
        },
        "players": {
            "max": mc.max_players,
            "online": len(mc.players),
            "sample": [
                {"name": player.name, "id": str(player.uuid)} for player in mc.players
            ]
        },
        "description": {
            "text": mc.motd,
            "underlined": True,
            "color": "white",
        },
        "previewsChat": False,
        "enforcesSecureChat": False,
    }
    if mc.favicon:
        json_data["favicon"] = "data:image/png;base64," + base64.b64encode(mc.favicon).decode()
    resp = json.dumps(json_data)
    return protocol.construct_packet(0, protocol.write_var_base(len(resp)) + resp.encode())


def handle_ping(data: bytes) -> bytes:
    return protocol.construct_packet(1, data)

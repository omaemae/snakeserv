import enum
import json

from mcnetty import protocol
from minecraft import Minecraft, Player


class Events(enum.Enum):
    DISCONNECT = 0x0
    LOGIN_START = 0x0
    LOGIN_SUCCESS = 0x2


def handle_login_start(data: bytes, mc: Minecraft):
    (name, data) = protocol.consume_string(data, 16)
    (has_sig_data, data) = protocol.consume_struct_format(data, '?')
    timestamp = None
    pub_key_len = None
    pub_key = None
    sig_len = None
    sig = None
    if has_sig_data:
        (timestamp, data) = protocol.consume_struct_format(data, 'q')
        (pub_key_len, data) = protocol.consume_varint(data)
        (pub_key, data) = protocol.consume_bytes(data, pub_key_len)
        (sig_len, data) = protocol.consume_varint(data)
        (sig, data) = protocol.consume_bytes(data, sig_len)
    has_player_uuid = False
    player_uuid = None
    if len(data) > 0:
        (has_player_uuid, data) = protocol.consume_struct_format(data, '?')
    if has_player_uuid:
        (player_uuid, data) = protocol.consume_uuid(data)
    print(f"received login start from player {name} (UUID={player_uuid}) (has sig data? {has_sig_data})")
    player = Player(name, player_uuid)
    mc.players.append(player)

    resp = player.uuid.bytes
    resp += protocol.write_var_base(len(name)) + name.encode()
    resp += protocol.write_var_base(0)  # number of properties (0)
    return protocol.construct_packet(Events.LOGIN_SUCCESS.value, resp), player


def send_disconnect(reason: str):
    return protocol.construct_packet(
        Events.DISCONNECT.value,
        protocol.construct_string(
            json.dumps({
                "text": reason
            })
        )
    )
